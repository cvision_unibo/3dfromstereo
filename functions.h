#pragma once
#include "opencv2\core.hpp"
#include "opencv2\calib3d.hpp"
#include "opencv2\highgui.hpp"
#include "opencv2\ximgproc.hpp"
#include <iostream>
#include "opencv2\viz.hpp"
#include <fstream>
#include "opencv2\imgproc.hpp"
#include <iomanip>

using namespace std;
using namespace cv;


class imgCouple
{
public:
	int nL = 0;
	int nR = 0;
	Mat left;
	Mat right;
	Mat map;
	Mat colors;
	Mat colorsProc;																	//qui vengono caricati solo i colori dei punti <zmax e >zmin (per avere color e _3d dello stesso size)
	Mat homogeneousLeft;															//trasformazione per il sistema di riferimento mondo
	Mat homogeneousRight;
	vector<Point3f> points_3d;

	void disparityMap(bool visual, float zmin, Mat &camera)
	{
		Mat output1;
		double fx = camera.at<double>(0, 0);
		//int minDisparity = round(((base()*606.12037 / zmax) / 16) + 1) * 16;
		int minDisparity = 0;
		int numDisparities = round((base()*fx / zmin) / 16) * 16 - minDisparity;	//deve essere multiplo di 16
		//int numDisparities = 160 - minDisparity;	//deve essere multiplo di 16
		int blocksize = 5;
		int visual_scale = 1;

		left.convertTo(left, CV_8UC1);
		right.convertTo(right, CV_8UC1);

		Ptr<StereoSGBM> stereo = StereoSGBM::create(minDisparity, numDisparities, blocksize, 8 * blocksize*blocksize, 32 * blocksize*blocksize, -1, 0, 10, 150, 2, StereoSGBM::MODE_HH);	//cut off MODE_HH for better performance
		stereo->compute(left, right, map);
													//map: real disparity; output1: disparity*16 (for visualization)
		if (visual)
		{
			ximgproc::getDisparityVis(map, output1, visual_scale);
			imshow("disparity map image " + to_string(nL) + "-" + to_string(nR), output1);
			waitKey(0);
		}
	}
	double base()
	{
		double x_distance = homogeneousRight.at<double>(0, 3) - homogeneousLeft.at<double>(0, 3);
		double y_distance = homogeneousRight.at<double>(1, 3) - homogeneousLeft.at<double>(1, 3);
		double z_distance = homogeneousRight.at<double>(2, 3) - homogeneousLeft.at<double>(2, 3);
		return sqrt(x_distance*x_distance + y_distance*y_distance + z_distance*z_distance);
	}
	void project3d(Mat &camera, float z_min, float z_max)
	{
		double fx = camera.at<double>(0, 0);
		double cx = camera.at<double>(0, 2);
		double fy = camera.at<double>(1, 1);
		double cy = camera.at<double>(1, 2);

		Point3f temp;
		for (int i = 0; i < map.rows; i++)
			for (int j = 0; j < map.cols; j++)
			{
				if (map.at<short>(i, j) != 0)
				{
					temp.z = base()*fx / (double (map.at<short>(i, j))/16.0);
					temp.x = (temp.z / fx)*((double)j - cx);
					temp.y = (temp.z / fy)*((double)i - cy);
					if (temp.z >= z_min && temp.z < z_max)
					{
						points_3d.push_back(temp);
						colorsProc.push_back(colors.at<Vec3b>(i, j));
					}
				}
			}
	}
	void visualize3d()
	{
		if (!points_3d.empty())
		{
			Mat _3d;
			_3d = Mat(points_3d);
			_3d.convertTo(_3d, CV_32FC3);

			viz::WCloud cloud(_3d, colorsProc);
			viz::Viz3d myWindow("3d " + to_string(nL) + "-" + to_string(nR));
			myWindow.showWidget("coordinate system", viz::WCoordinateSystem(0.1));
			myWindow.showWidget("3d rapresentation " + to_string(nL) + "-" + to_string(nR), cloud);
			myWindow.spin();
			//viz::writeCloud("../b=8_undistort.ply", _3d, colorsProc);
		}
		else
			cout << nL << "-" << nR << ": EMPTY 3D POINTS" << endl;
	}
private:
};

class pointcloud
{
public:
	Mat Composition;
	Mat Colors;

	void compose3d(imgCouple &input)
	{
		if (!input.points_3d.empty())
		{
			//convert input 3d points to homogeneous
			Mat homoPoints;
			convertPointsToHomogeneous(input.points_3d, homoPoints);
			homoPoints = homoPoints.reshape(1);

			//transform points to the world coordinate system
			Mat result;
			input.homogeneousLeft.convertTo(input.homogeneousLeft, CV_32FC1);
			homoPoints.convertTo(homoPoints, CV_32FC1);
			result = input.homogeneousLeft*homoPoints.t();
			result = result.t();
			result = result.reshape(4);

			//append to the cloud
			Composition.push_back(result);
			Colors.push_back(input.colorsProc);
		}
		
	}
	void visualize(char mode, double bmin, int sel, int vp)
	{
		int name = 0;
		viz::Viz3d myWindow("3d");
		
		viz::WCloud composition_3d(Composition, Colors);
		myWindow.showWidget("3d composition", composition_3d);
		myWindow.showWidget("coordinate system", viz::WCoordinateSystem(0.1));
		//Mat screen;
		myWindow.spinOnce(10000, true);
		while (!myWindow.wasStopped())
		{
			//screen = myWindow.getScreenshot();
			myWindow.spinOnce(10000, true);
		}
		//imwrite("../data/screenshot.jpg", screen);
		stringstream stream;
		stream << fixed << setprecision(2) << (sel + 1)*bmin;
		switch (mode)
		{
		case 's': 
			viz::writeCloud("./3d_comp_singlebase(b=" + stream.str() + "m)(vp=" + to_string(vp) + ").ply", Composition, Colors);
			break;
		case 'm': 
			viz::writeCloud("./3d_comp_multibase(vp=" + to_string(vp) + ").ply", Composition, Colors);
			break;
		}	
	}
private:
};

bool loadImages(string address, int vp, imgCouple &output, int a, int b);
void undistorsion(imgCouple &input, Mat &camera, Mat &distorsion);
bool readCameraIntrinsic(string address, Mat &camera, Mat &dist_param);
bool readCameraEstrinsic(string address, int vp, imgCouple &output);
bool readZlimit(string address, vector<double> &z, float zmax, float margin);