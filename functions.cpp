#include "functions.h"

using namespace std;
using namespace cv;


bool loadImages(string address, int vp, imgCouple &output, int a, int b){
	output.nL = a;
	output.nR = b;
	string strg;

	if (output.nL == 0)
		strg = address + "viewpoint_" + to_string(vp) + ".png";
	else
		strg = address + "viewpoint_" + to_string(vp) + "_dx_" + to_string(output.nL) + ".png";

	output.left = imread(strg, IMREAD_GRAYSCALE);
	if (output.left.empty())
		return false;

	output.right = imread(address + "viewpoint_" + to_string(vp) + "_dx_" + to_string(output.nR) + ".png", IMREAD_GRAYSCALE);
	if (output.right.empty())
		return false;

	output.colors = imread(strg, IMREAD_COLOR);

	return true;
}
void undistorsion(imgCouple &input, Mat &camera, Mat &distorsion)
{
	Mat tempL, tempR, tempCol;
	input.left.copyTo(tempL);
	input.right.copyTo(tempR);
	input.colors.copyTo(tempCol);

	//undistorsion
	undistort(tempL, input.left, camera, distorsion);
	undistort(tempR, input.right, camera, distorsion);
	undistort(tempCol, input.colors, camera, distorsion);
}
bool readCameraIntrinsic(string address, Mat &camera, Mat &dist_param)
{
	ifstream file(address + "cam_intrinsics.txt");
	if (!file.is_open())
	{
		cout << "Error opening the intrinsic camera file" << endl;
		system("pause");
		return false;
	}

	double fx, fy, cx, cy, k1, k2, t1, t2;
	file >> fx >> fy >> cx >> cy >> k1 >> k2 >> t1 >> t2;

	//make camera matrix
	camera = (Mat_<double>({ fx, 0, cx,
							 0, fy, cy,
							 0,  0,  1 })).reshape(1, 3);

	//make distorsion coefficient vector
	dist_param = (Mat_<double>({ k1, k2, t1, t2 })).reshape(1, 4);

	return true;
}
bool readCameraEstrinsic(string address, int vp, imgCouple &output)
{
	double T[3];
	double quat[4];

	//read files matrix_frame_L.txt for left image
	string strg;
	if (output.nL == 0)
		strg = address + "viewpoint_" + to_string(vp) + ".txt";
	else
		strg = address + "viewpoint_" + to_string(vp) + "_dx_" + to_string(output.nL) + ".txt";

	ifstream file(strg);
	if (!file.is_open())
	{
		cout << "Error opening the camera position files" << endl;
		system("pause");
		return false;
	}
		
	file >> T[0] >> T[1] >> T[2] >> quat[0] >> quat[1] >> quat[2] >> quat[3];
	
	//conversion from quaternion to rotation matrix
	double m00 = 1 - 2 * quat[1] * quat[1] - 2 * quat[2] * quat[2];
	double m11 = 1 - 2 * quat[0] * quat[0] - 2 * quat[2] * quat[2];
	double m22 = 1 - 2 * quat[0] * quat[0] - 2 * quat[1] * quat[1];
	double m01 = 2 * quat[0] * quat[1] - 2 * quat[2] * quat[3];
	double m10 = 2 * quat[0] * quat[1] + 2 * quat[2] * quat[3];
	double m02 = 2 * quat[0] * quat[2] + 2 * quat[1] * quat[3];
	double m20 = 2 * quat[0] * quat[2] - 2 * quat[1] * quat[3];
	double m12 = 2 * quat[1] * quat[2] - 2 * quat[0] * quat[3];
	double m21 = 2 * quat[1] * quat[2] + 2 * quat[0] * quat[3];
	
	output.homogeneousLeft = (Mat_<double>({ m00, m01, m02, T[0],
									     m10, m11, m12, T[1],
								    	 m20, m21, m22, T[2],
										   0,   0,   0,    1 })).reshape(1, 4);

	//read files matrix_frame_R.txt for right image
	ifstream file1(address + "viewpoint_" + to_string(vp) + "_dx_" + to_string(output.nR) + ".txt");
	if (!file1.is_open())
	{
		cout << "Error opening the camera position files" << endl;
		system("pause");
		return false;
	}

	file1 >> T[0] >> T[1] >> T[2] >> quat[0] >> quat[1] >> quat[2] >> quat[3];

	//conversion from quaternion to rotation matrix
	m00 = 1 - 2 * quat[1] * quat[1] - 2 * quat[2] * quat[2];
	m11 = 1 - 2 * quat[0] * quat[0] - 2 * quat[2] * quat[2];
	m22 = 1 - 2 * quat[0] * quat[0] - 2 * quat[1] * quat[1];
	m01 = 2 * quat[0] * quat[1] - 2 * quat[2] * quat[3];
	m10 = 2 * quat[0] * quat[1] + 2 * quat[2] * quat[3];
	m02 = 2 * quat[0] * quat[2] + 2 * quat[1] * quat[3];
	m20 = 2 * quat[0] * quat[2] - 2 * quat[1] * quat[3];
	m12 = 2 * quat[1] * quat[2] - 2 * quat[0] * quat[3];
	m21 = 2 * quat[1] * quat[2] + 2 * quat[0] * quat[3];

	output.homogeneousRight = (Mat_<double>({ m00, m01, m02, T[0],
         								    	m10, m11, m12, T[1],
												m20, m21, m22, T[2],
												0,   0,   0,    1 })).reshape(1, 4);

	return true;
}
bool readZlimit(string address, vector<double> &z, float zmax, float margin)
{
	ifstream file(address + "z_limits.txt");
	if (!file.is_open())
	{
		cout << "Error opening z_limits file" << endl;
		system("pause");
		return false;
	}
	
	double temp;
	while (file >> temp)
		z.push_back(temp + margin);		//aggiungo un margine di sicurezza
	
	z.push_back(zmax);

	return true;
}