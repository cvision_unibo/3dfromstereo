#include "functions.h"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])	//arg1=address; arg2=viewpoint number; arg3=minimum baseline(m); arg4=Zmin; arg5=Zmax; arg6=Zmargin; arg7=single\multi; arg8=selected baseline; 
{
	//arguments
	string address = argv[1];
	int viewpoint = atoi(argv[2]);
	double Bmin = atof(argv[3]);
	float margin = atof(argv[6]);
	char mode = *argv[7];
	int sel;
	if (mode == 's')
		sel = atoi(argv[8]) - 1;	//-1 perch� in Couples[0] c'� 0-1, in Couples[1] c'� 0-2...

	bool visualize = false;		//(debug)
	vector<double> Zlimits;		//fette di depth da proiettare

	float zmin = atof(argv[4]);
	float zmax = atof(argv[5]);
	vector<imgCouple> Couples;		//couples of images
	pointcloud Cloud;				//total 3d pointcloud
	Mat cam_intrinsics, dist_coeff;

									
	//image load
	imgCouple temp;
	bool load = true;
	int L = 0;
	int R = 1;
	while (load)
	{
		load = loadImages(address, viewpoint, temp, L, R);
		if (load)
			Couples.push_back(temp);	
		R++;	//left image=0, right image=1, 2, 3...
	}
	if (Couples.empty())
	{
		cout << "Error opening images" << endl;
		return -1;
	}
		
	//camera instrinsic load
	load = readCameraIntrinsic(address, cam_intrinsics, dist_coeff);
	if (!load)
		return -1;

	//camera estrinsic load
	for (vector<imgCouple>::iterator it = Couples.begin(); it != Couples.end(); ++it)
	{
		load = readCameraEstrinsic(address, viewpoint, *it);
		if (!load)
			return -1;
	}

	//z_limit.txt load
	load = readZlimit(address, Zlimits, zmax, margin);
	
	//undistorsion
	for (vector<imgCouple>::iterator it = Couples.begin(); it != Couples.end(); ++it)
	{
		undistorsion((*it), cam_intrinsics, dist_coeff);
	}

	//disparity map
	int j = 0;
	for (vector<imgCouple>::iterator it = Couples.begin(); it != Couples.end(); ++it, ++j)
		(*it).disparityMap(visualize, Zlimits[j], cam_intrinsics);
		
	
	int i = 0;
	switch (mode)
	{
	case 's':		//single mode: considerata solo la baseline inserita come argomento
		//3d projection
		Couples[sel].project3d(cam_intrinsics, 0.01, zmax);
		//3d composition
		Cloud.compose3d(Couples[sel]);
		break;

	case 'm':		//multi mode: composizione di ricostruzioni 3d da pi� baseline
		//3d projection
		for (vector<imgCouple>::iterator it = Couples.begin(); it != Couples.end(); ++it, ++i)
		{
			if (Zlimits[i] > zmin)
			{
				(*it).project3d(cam_intrinsics, Zlimits[i], Zlimits[i + 1]);
				if (visualize)
					(*it).visualize3d();
			}
		}
		//3d composition
		for (vector<imgCouple>::iterator it = Couples.begin(); it != Couples.end(); ++it)
		{
			Cloud.compose3d(*it);
		}
		break;

	default: 
		cout << "Enter a valid argument: m/s" << endl;
	}

	Cloud.visualize(mode, Bmin, sel, viewpoint);

	return 0;
}